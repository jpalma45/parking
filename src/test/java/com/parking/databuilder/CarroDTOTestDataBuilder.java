package com.parking.databuilder;

import com.parking.dto.CarroDTO;

public class CarroDTOTestDataBuilder {

	private String placa;
	
	public CarroDTOTestDataBuilder() {
		placa = "COT18S";
	}
	
	public CarroDTOTestDataBuilder withPlaca(String placa) {
		this.placa = placa;
		return this;
	}

	public CarroDTO build() {
		return new CarroDTO(placa);
	}
	
	public static CarroDTOTestDataBuilder aCarroDTO() {
		return new CarroDTOTestDataBuilder();
	}
}
