package com.parking.service.integration;

import static com.parking.databuilder.CarroDTOTestDataBuilder.aCarroDTO;
import static com.parking.databuilder.MotoDTOTestDataBuilder.aMotoDTO;

import java.time.LocalDateTime;
import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.parking.dto.CarroDTO;
import com.parking.dto.CeldaParqueaderoDTO;
import com.parking.dto.MotoDTO;
import com.parking.service.CeldaParqueaderoService;
import com.parking.service.VehiculoService;



@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CeldaParqueaderoServiceImplTest {

	@Autowired
	private CeldaParqueaderoService celdaParqueaderoService;

	@Autowired
	private VehiculoService<CarroDTO> carroService;

	@Autowired
	private VehiculoService<MotoDTO> motoService;
	
	@Test
	public void TestUnoGuardarCeldaParqueaderoCarroTest() {
		// Arrange
		CarroDTO carroDTO = carroService.guardarVehiculo(aCarroDTO().build());
		CeldaParqueaderoDTO celdaParqueaderoDTO = new CeldaParqueaderoDTO(LocalDateTime.now());
		celdaParqueaderoDTO.setCarro(carroDTO);

		// Act
		CeldaParqueaderoDTO resultado = celdaParqueaderoService.guardarCeldaParqueadero(celdaParqueaderoDTO);

		// Assert
		assertEquals(carroDTO.getPlaca(), resultado.getCarro().getPlaca());
		assertEquals(celdaParqueaderoDTO.getFechaIngreso(), resultado.getFechaIngreso());
	}
	
	@Test
	public void TestDosGuardarCeldaParqueaderoMotoTest() {
		// Arrange
		MotoDTO motoDTO = motoService.guardarVehiculo(aMotoDTO().build());
		CeldaParqueaderoDTO celdaParqueaderoDTO = new CeldaParqueaderoDTO(LocalDateTime.now());
		celdaParqueaderoDTO.setMoto(motoDTO);

		// Act
		CeldaParqueaderoDTO resultado = celdaParqueaderoService.guardarCeldaParqueadero(celdaParqueaderoDTO);

		// Assert
		assertEquals(motoDTO.getPlaca(), resultado.getMoto().getPlaca());
		assertEquals(motoDTO.getCilindraje(), resultado.getMoto().getCilindraje());
		assertEquals(celdaParqueaderoDTO.getFechaIngreso(), resultado.getFechaIngreso());
	}
	
}
