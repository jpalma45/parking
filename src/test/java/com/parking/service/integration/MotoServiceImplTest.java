package com.parking.service.integration;

import static com.parking.databuilder.MotoDTOTestDataBuilder.aMotoDTO;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.parking.dto.MotoDTO;
import com.parking.service.VehiculoService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MotoServiceImplTest {
	
	@Autowired
	private VehiculoService<MotoDTO> motoService;
	
	@Test
	public void guardarMotoTest() {
		//Arrange
		MotoDTO moto = aMotoDTO().build();
		
		//Act
		MotoDTO motoAgregado = motoService.guardarVehiculo(moto);
		
		//Assert
		assertEquals(moto.getPlaca(), motoAgregado.getPlaca());
		assertEquals(moto.getCilindraje(), motoAgregado.getCilindraje());
	}

	@Test
	public void obtenerMoto() {
		//Arrange
		MotoDTO moto = new MotoDTO("KIYQ90I", 150);
		
		//Act
		MotoDTO motoAgregado = motoService.guardarVehiculo(moto);
		MotoDTO motoObtenido = motoService.buscarVehiculoPorPlaca(motoAgregado.getPlaca());
		
		//Assert
		assertEquals(motoAgregado.getPlaca(), motoObtenido.getPlaca());
		assertEquals(motoAgregado.getCilindraje(), motoObtenido.getCilindraje());
	}
}
