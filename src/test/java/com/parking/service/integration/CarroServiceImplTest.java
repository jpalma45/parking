package com.parking.service.integration;

import static org.junit.Assert.assertEquals;
import static com.parking.databuilder.CarroDTOTestDataBuilder.aCarroDTO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.parking.dto.CarroDTO;
import com.parking.service.VehiculoService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CarroServiceImplTest {

	@Autowired
	private VehiculoService<CarroDTO> carroService;
	
	@Test
	public void guardarCarro() {
		//Arrange
		CarroDTO carro = aCarroDTO().build();
		
		//Act
		CarroDTO carroAgregado = carroService.guardarVehiculo(carro);
		
		//Assert
		assertEquals(carro.getPlaca(), carroAgregado.getPlaca());
	}
	
	@Test
	public void obtenerCarro() {
		//Arrange
		CarroDTO carro = new CarroDTO("LLOPZIU");
		
		//Act
		CarroDTO carroAgregado = carroService.guardarVehiculo(carro);
		CarroDTO carroObtenido= carroService.buscarVehiculoPorPlaca(carroAgregado.getPlaca());
		
		//Assert
		assertEquals(carroAgregado.getPlaca(), carroObtenido.getPlaca());
	}
}
