package com.parking.web;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.parking.dominio.Carro;
import com.parking.repositorio.CarroRepository;
import com.parking.service.VigilanteService;


@RestController
@RequestMapping("vigilante")
@CrossOrigin(origins="http://localhost:4200",allowedHeaders="*")
public class VigilanteRestController {

	@Autowired
	private VigilanteService vigilanteService;
	
	@Autowired
	private CarroRepository carroRepository;
	
	@GetMapping("/registrarIngresoVehiculo")
	public String ingresarMoto(@RequestParam String placa, @RequestParam(defaultValue = "0") int cilindraje) {
		return vigilanteService.registrarIngresoVehiculo(placa, cilindraje, LocalDateTime.now());
	}
	
	@GetMapping("/cobrarRetiroVehiculo")
	public String cobrarRetiroCarro(@RequestParam String placa) {
		return vigilanteService.cobrarRetiroVehiculo(placa, LocalDateTime.now());
	}
	
	@GetMapping("/vehiculo")
	public List<Carro> findAll(@RequestParam(required=false) String placa){
		List<Carro> carros = new ArrayList<>();
		
		if (placa == null) {
			Iterable<Carro> results = this.carroRepository.findAll();
			results.forEach(carro -> {
				carros.add(carro);
			});
		}else {
			Carro carro = this.carroRepository.findByPlaca(placa);
			
			if (carro != null) {
				carros.add(carro);
			}
		}
		return carros;
	}
}
