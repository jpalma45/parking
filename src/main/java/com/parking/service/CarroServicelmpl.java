package com.parking.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.parking.dominio.Carro;
import com.parking.dto.CarroDTO;
import com.parking.repositorio.CarroRepository;

@Service("carroService")
public class CarroServicelmpl implements VehiculoService<CarroDTO>{

	@Autowired
	private CarroRepository carroRepository;
	
	@Autowired
	private UtilMapperService utilMapperService;

	@Override
	public CarroDTO guardarVehiculo(CarroDTO carroDTO) {
		Carro carro = utilMapperService.convertirDeClase1aClase2(carroDTO, CarroDTO.class, Carro.class);
		Carro carroGuardado = carroRepository.save(carro);
		return utilMapperService.convertirDeClase1aClase2(carroGuardado, Carro.class, CarroDTO.class);
	}

	@Override
	public CarroDTO buscarVehiculoPorPlaca(String placa) {
		Carro carro = carroRepository.findByPlaca(placa);
		return utilMapperService.convertirDeClase1aClase2(carro, Carro.class, CarroDTO.class);
	}
	
}
