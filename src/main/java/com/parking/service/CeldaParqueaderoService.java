package com.parking.service;

import java.util.List;

import com.parking.dto.CeldaParqueaderoDTO;

public interface CeldaParqueaderoService {

	CeldaParqueaderoDTO guardarCeldaParqueadero(CeldaParqueaderoDTO celdaParqueaderoDTO);
	
	List<CeldaParqueaderoDTO> buscarVehiculosParqueados();
}
