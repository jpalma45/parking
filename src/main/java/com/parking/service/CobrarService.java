package com.parking.service;

import com.parking.dto.CeldaParqueaderoDTO;

public interface CobrarService {

	int calcularValorRetiroVehiculo(CeldaParqueaderoDTO eldaParqueaderoDTO, double valorHora, double valorDia,
			int maxHorasValorHora, int maximoCilindraje, int valorImpuestoCilindraje);
}
