package com.parking.service;

import com.parking.dto.VehiculoDTO;

public interface VehiculoService <T extends VehiculoDTO>{

	T guardarVehiculo(T vehiculoDTO);

	T buscarVehiculoPorPlaca(String placa);
}
