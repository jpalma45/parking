package com.parking.dto;

public class CarroDTO extends VehiculoDTO{
	
	protected CarroDTO() {
	}
	
	public CarroDTO(String placa) {
		super(placa);
	}
}
