package com.parking.dominio;

import javax.persistence.Entity;

@Entity
public class Carro extends Vehiculo{

	protected Carro() {
		
	}

	public Carro(String placa) {
		super(placa);
	}
}
