package com.parking.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.parking.dominio.Vehiculo;

@NoRepositoryBean
public interface VehiculoRepository <T extends Vehiculo> extends JpaRepository<T, Long> {

	T findByPlaca(String placa);
}
