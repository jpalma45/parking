package com.parking.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.parking.dominio.CeldaParqueadero;

public interface CeldaParqueaderoRepository extends JpaRepository<CeldaParqueadero, Long>{

	List<CeldaParqueadero> findByFechaRetiroIsNull();
}
